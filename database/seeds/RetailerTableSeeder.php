<?php
/**
 * Created by PhpStorm.
 * User: simony
 * Date: 10/04/15
 * Time: 7:08 PM
 */
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class RetailerTableSeeder extends Seeder {

    public function run()
    {
        DB::table('retailers')->truncate();

        \App\Models\Retailer::insert(['name' => 'Coles', 'discount' => '0.95', 'sort' => 1]);
        \App\Models\Retailer::insert([
            ['name' => 'Woolworths', 'sort' => 2],
            ['name' => 'Costco', 'sort' => 5],
            ['name' => 'Chemist Warehouse', 'sort' => 10],
            ['name' => 'My Chemist', 'sort' => 11]
        ]);
    }
}