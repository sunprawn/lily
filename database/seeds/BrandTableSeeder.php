<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class BrandTableSeeder extends Seeder {

    public function run()
    {
        DB::table('brands')->truncate();

        \App\Models\Brand::insert([
            ['name' => 'Swisse', 'description' => '维他命'],
            ['name' => 'Blackmores', 'description' => '维他命'],
            ['name' => 'Aptamil', 'description' => '奶粉'],
            ['name' => 'Karicare', 'description' => '奶粉'],
            ['name' => 'Bellamys', 'description' => '奶粉']
        ]);
    }
}

