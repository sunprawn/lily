<?php
/**
 * Created by PhpStorm.
 * User: simony
 * Date: 9/04/15
 * Time: 11:38 PM
 */
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ProductTableSeeder extends seeder {

    public function run()
    {
        DB::table('products')->truncate();

        \App\Models\Product::insert([
            ['barcode' => '654616543246534', 'name' => 'A2 Step 2', 'brand_id' => '1', 'size' => '900 G', 'stock' => 0],
            ['barcode' => '789725786727', 'name' => 'A2 Step 3', 'brand_id' => '2', 'size' => '900 G', 'stock' => 0],
            ['barcode' => '278972577896', 'name' => 'Karicare Step 2', 'brand_id' => '3', 'size' => '900 G', 'stock' => 0],
            ['barcode' => '65461654787973246534', 'name' => 'Karicare Step 3', 'brand_id' => '3', 'size' => '900 G', 'stock' => 0],
            ['barcode' => '72867278677', 'name' => 'Bellemy Step 2', 'brand_id' => '4', 'size' => '900 G', 'stock' => 0]
        ]);
    }
}