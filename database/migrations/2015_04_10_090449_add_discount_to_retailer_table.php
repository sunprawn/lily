<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDiscountToRetailerTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('retailers', function(Blueprint $table)
		{
			$table->decimal('discount')->after('name')->default(1.00);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('retailers', function(Blueprint $table)
		{
            $table->dropColumn('discount');
		});
	}

}
