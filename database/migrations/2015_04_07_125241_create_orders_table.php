<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        Schema::create('orders', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('tracking')->unique();
            $table->string('name');
            $table->decimal('cost')->nullable();
            $table->decimal('shipping');
            $table->decimal('paid');
            $table->decimal('profit')->nullable();
            $table->date('dispatch');
            $table->boolean('received');
            $table->enum('flag',['red', 'yellow','green'])->nullable();
            $table->string('memo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }

}
