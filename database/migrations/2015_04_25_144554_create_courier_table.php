<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourierTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('couriers', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->string('website')->nullable();
            $table->string('url')->nullable();
            $table->string('param')->nullable();
            $table->integer('sort')->default(99);
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('couriers');
	}

}
