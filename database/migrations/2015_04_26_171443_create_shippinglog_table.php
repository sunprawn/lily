<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShippingLogTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('shipping_logs', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('order_id');
            $table->dateTime('time')->nullable();
            $table->string('location')->nullable();
            $table->string('event')->nullable();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('shipping_logs');
	}

}
