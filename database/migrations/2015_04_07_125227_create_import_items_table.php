<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImportItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        Schema::create('import_items', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('import_id');
            $table->integer('product_id');
            $table->integer('quantity');
            $table->integer('left');
            $table->decimal('price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('import_items');
    }

}
