<?php namespace App\Console\Commands;

use App\Models\Order;
use App\Models\ShippingLog;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ShippingCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'updateShipping';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Update the shipping information for each order';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        $xpathQuery = [
            1 => '//div[@id="oDetail"]/table//tr[last()]/td',   // ARK express
            2 => '//div[contains(concat(" ", @class, " "), "track-right")]/table/tbody//tr[last()]/td',   // EWE express
        ];

        $matchingTable = [
            1 => [1, 2],                // same pattern
        ];

        $orders = Order::where('received', '!=', 1)->get();

        foreach($orders as $order) {

            $this->info("[ " . date('Y-m-d H:i:s') . " ] processing:  " . $order->tracking);

            /*
             * get result page with different courier
             */
            try {
                /*switch ($order->courier->id) {
                    case 1:     // ARK express
                        $url = $order->courier->url . $order->tracking;
                        $result = $this->getContentFromGet($url);
                        break;

                    case 2:     // EWE
                        $field = 'w=everfast&cno=' . $order->tracking;
                        $result = $this->getContentFromPost($order->courier->url, 2, $field);
                        break;

                    default:
                        $result = false;
                        break;
                }*/
                $url = $order->courier->url . $order->tracking;
                $result = $this->getContentFromGet($url);

            } catch (\Exception $e) {
                $this->error($e->getMessage());
                continue;
            }


            /*
             * if no result or no match xpath query
             */
            if (!$result || !isset($xpathQuery[$order->courier->id])) {
                continue;
            }

            //$result = mb_convert_encoding(file_get_contents($url), "utf-8", 'gb2312');
            $dom = new \DOMDocument();
            @$dom->loadHTML($result);
            $xpath = new \DOMXPath($dom);

            $data = $xpath->query($xpathQuery[$order->courier->id]);

            /*
             * For 方舟速递 and EWE
             */
            if($data->length == 3 && in_array($order->courier->id, $matchingTable[1])) {
                $shipping = ShippingLog::firstOrNew([
                    'order_id'  => $order->id,
                    'time'      => $data->item(0)->nodeValue,
                    'location'  => $data->item(1)->nodeValue,
                    'event'     => $data->item(2)->nodeValue,
                ]);

                $shipping->save();
            }

            if (strpos($shipping->event, "签收") !== false) {

                $order->received = true;
                $order->save();
            }
            if (!app()->environment('local')) {
                // The environment is local
                //$this->info($shipping->event);
                sleep(3);
            }


        }

	}

    protected function getContentFromPost( $url,  $count, $field_string) {
        //open connection
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST, $count);
        curl_setopt($ch,CURLOPT_POSTFIELDS, $field_string);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        //curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        //execute post
        $result = curl_exec($ch);

        //close connection
        curl_close($ch);

        return $result;
    }

    protected function getContentFromGet($url) {
        return file_get_contents($url);
    }

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			//['example', InputArgument::REQUIRED, 'An example argument.'],
            ['example', InputArgument::OPTIONAL, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
		];
	}

}
