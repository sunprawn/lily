<?php
/**
 * Created by PhpStorm.
 * User: simony
 * Date: 3/06/15
 * Time: 9:29 PM
 */

namespace App\Services;


class Helper {

    /**
     * detect mobile
     * @return bool
     */
    public static function isMobile() {
        $useragent=$_SERVER['HTTP_USER_AGENT'];

        if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|elaine|ip(hone|od)|iris|kindle|mmp|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|up\.(browser|link)|vodafone|wap|windows (ce|phone)/i',$useragent))
            return true;
        else
            return false;
    }

}