<?php
/**
 * Created by PhpStorm.
 * User: simony
 * Date: 9/04/15
 * Time: 10:09 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\ImportItem;

class OrderItem extends Model {

    public function order()
    {
        return $this->belongsTo('App\Models\Order');
    }

    public function importItems()
    {
        return $this->belongsToMany('App\Models\ImportItem', 'order_import', 'order_item_id', 'import_item_id')->withTimestamps();
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }

    public function calculateImportItems()
    {
        $requestQuantity = $this->quantity;

        if ($this->product->stock < $this->quantity) {
            throw new \Exception('Not enough quantity');
        }

        while ($requestQuantity) {

            // get the first available import_item
            $import_item = ImportItem::byPID($this->product->id)
                                        ->where('left', '>', 0)
                                        ->orderBy('created_at')
                                        ->first();

            if (!$import_item) {
                throw new \Exception('Not enough quantity left');
            }

            if ($import_item->left >= $requestQuantity) {

                // update the order_item
                $this->cost += $requestQuantity * $import_item->price;
                $this->importItems()->attach($import_item->id);

                // update and save the import item
                $import_item->left -= $requestQuantity;

                // change request quantity
                $requestQuantity = 0;

                $import_item->save();
            }
            else {

                // update the order_item
                $this->cost += $import_item->left * $import_item->price;
                $this->importItems()->attach($import_item->id);

                // change request quantity
                $requestQuantity -= $import_item->left;

                // update and save the import item
                $import_item->left = 0;
                $import_item->save();
            }
        }

        $this->product->stock -= $this->quantity;
        //$this->product()-save();
        $this->push();
    }
}