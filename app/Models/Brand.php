<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model {

    protected $fillable = ['name', 'description', 'sort'];

    public $timestamps = false;

    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }

    public function importItems()
    {
        return $this->hasManyThrough('App\Models\ImportItem', 'App\Models\Product');
    }

    public function orderItems()
    {
        return $this->hasManyThrough('App\Models\OrderItem', 'App\Models\Product');
    }
}