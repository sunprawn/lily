<?php
/**
 * Created by PhpStorm.
 * User: simony
 * Date: 25/04/15
 * Time: 2:58 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Courier extends Model {

    protected $fillable = ['name', 'website', 'url', 'sort'];

    public $timestamps = false;

    public function orders()
    {
        return $this->hasMany('App\Models\Orders');
    }

}