<?php
/**
 * Created by PhpStorm.
 * User: simony
 * Date: 11/04/15
 * Time: 3:01 PM
 */

namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class Customer extends Model{

    protected $fillable = ['name', 'phone', 'address', 'identity'];

    public function orders()
    {
        return $this->hasMany('App\Models\Order');
    }
}