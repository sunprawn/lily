<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

class Order extends Model {

    protected $fillable = ['tracking', 'shipping', 'paid', 'dispatch'];

    public function getReceivedAttribute($value) {
        return $value?true:false;
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer');
    }

    public function courier()
    {
        return $this->belongsTo('App\Models\Courier');
    }

    public function orderItems()
    {
        return $this->hasMany('App\Models\OrderItem');
    }

    public function shippingLogs()
    {
        return $this->hasMany('App\Models\ShippingLog');
    }

    public function lastShippingLog()
    {
        return $this->hasOne('App\Models\ShippingLog')->orderBy('id', 'desc')->select('id', 'order_id', 'event');
    }
}