<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Import extends Model {

    public $timestamps = false;

    public function importItems()
    {
        return $this->hasMany('App\Models\ImportItem');
    }

    public function retailer()
    {
        return $this->belongsTo('App\Models\Retailer');
    }
}