<?php
/**
 * Created by PhpStorm.
 * User: simony
 * Date: 9/04/15
 * Time: 10:09 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImportItem extends Model {

    protected $fillable = ['quantity', 'left', 'price'];

    public function import()
    {
        return $this->belongsTo('App\Models\Import');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }

    public function orderItems()
    {
        return $this->belongsToMany('App\Models\OrderItem', 'order_import', 'import_item_id', 'order_item_id')->withTimestamps();
    }

    public function scopeByPID($query, $pid)
    {
        return $query->where('product_id', $pid);
    }
}