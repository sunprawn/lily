<?php
/**
 * Created by PhpStorm.
 * User: simony
 * Date: 13/04/15
 * Time: 12:08 AM
 */

namespace App\Http\Middleware;

use App\Models\Setting;
use Closure;
use Illuminate\Contracts\Routing\Middleware;
use Illuminate\Support\Facades\Session;

class RegisterSettings{

    public function handle($request, Closure $next)
    {
        $settings = Setting::all();

        foreach ($settings as $setting) {
            Session::put($setting->key, $setting->value);
        }
        return $next($request);
    }

}