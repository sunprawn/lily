<?php namespace App\Http\Controllers;

use App\Http\Requests;

use App\Models\Brand;
use App\Models\Product;
use App\Models\Retailer;
use Illuminate\Http\Request;
use App\Models\Import;
use App\Models\ImportItem;
use Illuminate\Support\Facades\DB;

class ImportController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $items = ImportItem::orderBy('import_id', 'desc')->paginate(session('page')?session('page'):50);
        return view('imports.index')->with('items', $items);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $retailers = Retailer::orderBy('sort')->get();
        $brands = Brand::orderBy('sort')->get();
		return view('imports.create')->with('retailers', $retailers)->with('brands', $brands);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        if(!$request->isJson()) {
            return redirect()->back()->withInput();;
        }

        $result = [];

        try {
            DB::beginTransaction();

            $retailer = Retailer::findOrFail($request->json('retailer'));

            $import = new Import();
            $import->retailer()->associate($retailer);
            $import->purchase_date = $request->json('purchased');

            /*
             * save the import with related retailer
             */
            //$retailer->imports->save($import);
            $import->save();

            foreach($request->json('items') as $item) {
                $product = Product::where('barcode', $item['barcode'])->first();
                $brand = Brand::find($item['brand']);

                /*
                 * if product not exist, create the product
                 */
                if($product == null) {
                    $product = Product::create([
                        'barcode'   => $item['barcode'],
                        'name'      => $item['name'],
                        'size'      => $item['size']
                    ]);
                    $product->brand()->associate($brand);
                    $product->save();
                }

                $importItem = ImportItem::create([
                    'quantity'  => $item['quantity'],
                    'left'     => $item['quantity'],
                    'price'     => $item['price'] * $retailer->discount
                ]);

                $importItem->product()->associate($product);
                $importItem->import()->associate($import);
                $importItem->save();

                $product->stock += $item['quantity'];
                $product->save();
            }
            //throw new Exception("wrong");
            DB::commit();

            $result = [
                'error'     => '',
                'id'  => $import->id
            ];

        } catch (\Exception $e){

            $result['error'] = $e->getMessage();
            DB::rollback();

        }

        return response()->json($result);

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$import = Import::find($id);

        //var_dump($import->importItems);

        return view('imports.show')->with('import', $import);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
