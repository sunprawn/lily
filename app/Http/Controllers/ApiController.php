<?php
/**
 * Created by PhpStorm.
 * User: simony
 * Date: 14/04/15
 * Time: 12:04 AM
 */

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\ImportItem;
use App\Models\Product;

class ApiController extends Controller{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function order() {
        $orders = Order::select('id','tracking','shipping','cost','paid','profit','dispatch', 'received','customer_id','courier_id')
            ->orderBy('dispatch', 'desc')
            ->with(['customer' => function($query) {
                $query->select('id', 'name');
            }])
            ->with(['courier' => function($query) {
                $query->select('id', 'url');
            }])
            ->with('lastShippingLog')
            //->with(['shippingLogs' => function($query) {
            //    $query->orderBy('time','desc')->select('id', 'order_id', 'event')->take(1);
            //}])
            ->paginate(session('page')?session('page'):50);

        return response()->json($orders);
    }

    public function import() {
        $items = ImportItem::orderBy('import_id', 'desc')
            ->with(['product' => function($query) {
                $query->select('id', 'barcode', 'name','brand_id')->with('brand');
            }])
            ->paginate(session('page')?session('page'):50);

        return response()->json($items);
    }

    public function product() {
        $product = Product::orderBy('stock', 'desc')
            ->with('brand')
            ->paginate(session('page')?session('page'):50);

        return response()->json($product);
    }
}