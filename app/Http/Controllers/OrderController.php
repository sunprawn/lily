<?php namespace App\Http\Controllers;

use App\Models\Courier;
use App\Models\Customer;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Product;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

        $orders = Order::orderBy('dispatch', 'desc')->paginate(session('page')?session('page'):50);
        return view('orders.index')->with('orders', $orders);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        //$retailers = Retailer::orderBy('sort')->get();
        //$brands = Brand::orderBy('sort')->get();
        $couriers = Courier::orderBy('sort')->get();
        return view('orders.create')->with('couriers', $couriers);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        if(!$request->isJson()) {
            return redirect()->back()->withInput();;
        }

        $result = [];
        $inputCustomer = $request->json('customer');

        try {
            DB::beginTransaction();

            /*
             * get the customer or create new
             */
            $customer = Customer::firstOrCreate([
                'name'      => $inputCustomer['name'],
                'phone'     => $inputCustomer['phone'],
                'address'   => $inputCustomer['address'],
                'identity'  => $inputCustomer['identity']
            ]);

            /*
             * get the courier
             */
            $courier = Courier::findOrFail($request->json('courier'));

            /*
             * calculate paid amount
             */
            $paidAmount = 0;
            if (!$request->json('paid')) {
                foreach($request->json('items') as $item) {
                    $paidAmount += $item['quantity'] * $item['price'] + $item['postage'];
                }
            } else {
                $paidAmount = $request->json('paid');
            }

            /*
             * Create new order
             */
            $order = Order::create([
                'tracking'  => $request->json('tracking'),
                'shipping'  => $request->json('shipping'),
                'paid'      => $paidAmount,
                'dispatch'  => $request->json('dispatch')
            ]);

            $order->courier()->associate($courier);
            $order->customer()->associate($customer);
            $order->save();

            /*
             *  Create order items
             */
            foreach($request->json('items') as $item) {
                $product = Product::where('barcode', $item['barcode'])->first();

                $orderItem = new OrderItem();
                $orderItem->product()->associate($product);
                $orderItem->order()->associate($order);
                $orderItem->quantity = $item['quantity'];
                $orderItem->price = $item['price'];
                $orderItem->postage = $item['postage'];
                $orderItem->save();
                $orderItem->calculateImportItems();

                $order->cost += $orderItem->cost;
            }

            $order->profit = $order->paid - ($order->cost + $order->shipping) * (session('rate')?session('rate'):6);
            $order->save();

            $result = [
                'error'     => '',
                'orderID'   => $order->id,
            ];

            DB::commit();
        } catch (\Exception $e) {

            $result['error'] = $e->getMessage();
            DB::rollback();
        }

        return response()->json($result);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id, Request $request)
	{
        $order = Order::with('customer')
            ->with('courier')
            ->with('orderItems')
            ->with('shippingLogs')
            ->find($id);


        // load the related product
        foreach ($order->orderItems as $key => $item) {
            $item->product->brand = $item->product->brand;
            $order->orderItems[$key]->product = $item->product;
        }

        if ($request->input('type') == 'json') {
            return response()->json($order);
        } else {

            $orderJson = $order->toJson();
            return view('orders.show')->with('order', $orderJson);
        }

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Request $request)
	{

        try {
            $order = Order::findOrFail($id);

            if (null !== $request->json('received')) {
                $order->received = $request->json('received')=='true'?1:0;
            }

            if (null !== $request->json('memo')) {
                $order->memo = $request->json('memo');
            }

            $order->save();
            $result = [
                'status'    => 'success',
                'error'     => '',
            ];

        } catch (\Exception $e) {
            $result = ['error' => $e->getMessage()];
        }

        return response()->json($result);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
