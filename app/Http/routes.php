<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::resource('product', 'ProductController',['except' => ['destroy']]);
Route::resource('order', 'OrderController',['except' => ['destroy']]);
Route::resource('import', 'ImportController',['except' => ['destroy']]);
Route::resource('brand', 'BrandController',['except' => ['destroy']]);
Route::resource('retailer', 'RetailerController',['except' => ['destroy']]);
Route::resource('customer', 'CustomerController',['except' => ['destroy']]);
Route::resource('courier', 'CourierController',['except' => ['destroy']]);

Route::get('product/bar/{barcode}', 'ProductController@bar');
Route::get('customer/get/{name}/{identity}', 'CustomerController@get');
Route::get('api/order', 'ApiController@order');
Route::get('api/import', 'ApiController@import');
Route::get('api/product', 'ApiController@product');
