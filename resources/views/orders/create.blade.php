@extends('app')


@section('content')
    <script src="{{ asset('/js/jquery-ui.min.js') }}"></script>
    <link href="{{ asset('/css/jquery-ui.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/jquery-ui.theme.min.css') }}" rel="stylesheet">

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @if(!Helper::isMobile())
                <div class="panel panel-default">
                    <div class="panel-heading"><h3 class="panel-title">Create Order</h3></div>

                    <div class="panel-body">
                @else
                    <h3 class="panel-title">Create Order</h3><br />
                @endif
                        <div ng-app="orderApp" ng-controller="formCtrl">
                            <form name="orderForm" ng-submit="submitForm(orderForm.$valid)" class="form-horizontal" novalidate>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <!-- ============ Order detail ============ -->

                                <div class="form-group">
                                    <label for="inputTracking" class="col-sm-3 control-label">Tracking</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" id="inputTracking" name="tracking" placeholder="Tracking ID"
                                               ng-model="order.tracking" required>
                                    </div>
                                    <label class="col-sm-4 control-label" style="color:red;text-align:left;"
                                           ng-show="orderForm.tracking.$invalid && orderForm.tracking.$dirty">
                                        Tracking ID is required
                                    </label>
                                </div>

                                <div class="form-group">
                                    <label for="inputDate" class="col-sm-3 control-label">Dispatch Date</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" id="inputDate" name="dispatch" placeholder="Dispatch Date"
                                               ng-model="order.dispatch" required>
                                    </div>
                                    <label class="col-sm-4 control-label" style="color:red;text-align:left;"
                                           ng-show="orderForm.dispatch.$invalid && orderForm.dispatch.$dirty">
                                        Dispatch date is required
                                    </label>
                                </div>

                                <div class="form-group">
                                    <label for="inputShipping" class="col-sm-3 control-label">Shipping Cost</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" id="inputShipping" name="shipping" placeholder="Shipping Cost"
                                               ng-model="order.shipping" ng-pattern="/^\d+(\.\d{1,2})?$/" required>
                                    </div>
                                    <label class="col-sm-4 control-label" style="color:red;text-align:left;"
                                           ng-show="orderForm.shipping.$invalid && orderForm.shipping.$dirty">
                                        Shipping cost is invalid
                                    </label>
                                </div>

                                <div class="form-group">
                                    <label for="inputPaid" class="col-sm-3 control-label">Paid</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" id="inputPaid" name="paid" placeholder="Amount Paid (RMB)"
                                               ng-model="order.paid" ng-pattern="/^\d+(\.\d{1,2})?$/">
                                    </div>
                                    <label class="col-sm-4 control-label" style="color:red;text-align:left;"
                                           ng-show="orderForm.paid.$invalid && orderForm.paid.$dirty">
                                        Amount paid is invalid
                                    </label>
                                </div>

                                <div class="form-group">
                                    <label for="inputCourier" class="col-sm-3 control-label">Courier</label>
                                    <div class="col-sm-5">
                                        <select class="form-control" ng-model="order.courier" required="">
                                            @foreach( $couriers as $courier)
                                                <option value="{{ $courier->id }}">{{ $courier->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <label class="col-sm-4 control-label" style="color:red;text-align:left;"
                                           ng-show="orderForm.courier.$invalid && orderForm.courier.$dirty">
                                        Courier is required
                                    </label>
                                </div>
                                <hr />

                                <!-- ============ Customer session ============ -->

                                <div class="form-group">
                                    <label for="inputName" class="col-sm-3 control-label">Customer Name</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" id="inputName" name="name" placeholder="Customer Name"
                                               ng-model="order.customer.name" required>
                                    </div>
                                    <label class="col-sm-4 control-label" style="color:red;text-align:left;"
                                           ng-show="orderForm.name.$invalid && orderForm.name.$dirty">
                                        Customer Name is invalid
                                    </label>
                                </div>

                                <div class="form-group">
                                    <label for="inputIdentity" class="col-sm-3 control-label">Identity</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" id="inputIdentity" name="identity" placeholder="身份证 Identity"
                                               ng-model="order.customer.identity"  ng-blur="getCustomer()" required >
                                    </div>
                                    <label class="col-sm-4 control-label" style="color:red;text-align:left;"
                                           ng-show="orderForm.identity.$invalid && orderForm.identity.$dirty">
                                        Identity is invalid
                                    </label>
                                </div>

                                <div class="form-group">
                                    <label for="inputPhone" class="col-sm-3 control-label">Phone</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" id="inputPhone" name="phone" placeholder="Phone Number"
                                               ng-model="order.customer.phone" required>
                                    </div>
                                    <label class="col-sm-4 control-label" style="color:red;text-align:left;"
                                           ng-show="orderForm.phone.$invalid && orderForm.phone.$dirty">
                                        Phone is invalid
                                    </label>
                                </div>

                                <div class="form-group">
                                    <label for="inputAddress" class="col-sm-3 control-label">Delivery Address</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" id="inputAddress" name="address" placeholder="Delivery Address"
                                               ng-model="order.customer.address" required>
                                    </div>
                                    <label class="col-sm-4 control-label" style="color:red;text-align:left;"
                                           ng-show="orderForm.address.$invalid && orderForm.address.$dirty">
                                        Delivery Address is invalid
                                    </label>
                                </div>

                                <hr />
                            </form>

                            <!-- ============ Order item session ============ -->

                            <form name="orderItemForm" ng-submit="submitForm(orderItemForm.$valid)" novalidate>
                                @if(!Helper::isMobile())
                                <table class="table">
                                    <thead>
                                        <th>Barcode</th>
                                        <th>Brand</th>
                                        <th>Name</th>
                                        <th>Quantity</th>
                                        <th>Unit Price</th>
                                        <th>Postage</th>
                                        <th></th>
                                    </thead>
                                    <tbody>

                                        <tr ng-repeat="item in order.items">
                                            <input type="hidden" name="type[]" ng-model="item.type">
                                            <td class="col-xs-3">
                                                <div class="form-group form-table-group">
                                                    <label for="inputBarcode" class="sr-only">Product Barcode</label>
                                                    <input type="text" class="form-control" id="inputBarcode" name="barcode[]" placeholder="Product Barcode"
                                                           ng-model="item.barcode" ng-blur="getProduct($index)">
                                                </div>
                                            </td>

                                            <td>
                                                <div class="form-group form-table-group">
                                                    <label for="inputBrand" class="sr-only">Brand</label>
                                                    <p class="form-control-static" ng-model="item.brand">[[item.brand]]</p>
                                                </div>
                                            </td>

                                            <td>
                                                <div class="form-group form-table-group">
                                                    <label for="inputName" class="sr-only">Product Name</label>
                                                    <p class="form-control-static" ng-model="item.name">[[item.name]]</p>
                                                </div>
                                            </td>

                                            <td>
                                                <div class="form-group form-table-group">
                                                    <label for="inputQuantity" class="sr-only">Quantity</label>
                                                    <div style="width:95px">
                                                    <input type="text" class="form-control" id="inputQuantity" name="quantity[]" placeholder="Quantity"
                                                           ng-model="item.quantity" ng-pattern="/^\d+$/" required="">
                                                    </div>
                                                </div>
                                            </td>

                                            <td>
                                                <div class="form-group form-table-group">
                                                    <label for="inputPrice" class="sr-only">Unit Price</label>
                                                    <div style="width:95px">
                                                    <input type="text" class="form-control" id="inputPrice" name="price[]" placeholder="Unit Price"
                                                           ng-model="item.price" ng-pattern="/^\d+(\.\d{1,2})?$/" required="">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group form-table-group">
                                                    <label for="inputQuantity" class="sr-only">Postage</label>
                                                    <div style="width:85px">
                                                    <input type="text" class="form-control" id="inputQuantity" name="postage[]" placeholder="Postage"
                                                           ng-model="item.postage" ng-pattern="/^\d+(\.\d{1,2})?$/" required="">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group form-table-group" ng-show="!$first">
                                                    <button type="button" class="btn btn-default" ng-click="remove($index)">
                                                        <span class="glyphicon glyphicon-remove"></span>
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                                @else
                                <div ng-repeat="item in order.items">
                                    <div class="form-group form-table-group">
                                        <label for="inputBarcode" class="control-label">Product Barcode</label>
                                        <input type="text" class="form-control" id="inputBarcode" name="barcode[]" placeholder="Product Barcode"
                                               ng-model="item.barcode" ng-blur="getProduct($index)">
                                    </div>
                                    <!--div class="form-group form-table-group">
                                        <label for="inputBrand" class="control-label">Brand</label>
                                        <p class="form-control-static" ng-model="item.brand">[[item.brand]]</p>
                                    </div-->
                                    <div class="form-group form-table-group">
                                        <label for="inputName" class="control-label">Product Name</label>
                                        <p class="form-control-static" ng-model="item.name">[[item.name]]</p>
                                    </div>
                                    <div class="form-group form-table-group">
                                        <label for="inputQuantity" class="control-label">Quantity</label>
                                        <input type="text" class="form-control" id="inputQuantity" name="quantity[]" placeholder="Quantity"
                                               ng-model="item.quantity" ng-pattern="/^\d+$/" required="">
                                    </div>
                                    <div class="form-group form-table-group">
                                        <label for="inputPrice" class="control-label">Unit Price</label>
                                        <input type="text" class="form-control" id="inputPrice" name="price[]" placeholder="Unit Price"
                                               ng-model="item.price" ng-pattern="/^\d+(\.\d{1,2})?$/" required="">
                                    </div>
                                    <div class="form-group form-table-group">
                                        <label for="inputQuantity" class="control-label">Postage</label>
                                        <input type="text" class="form-control" id="inputQuantity" name="postage[]" placeholder="Postage"
                                               ng-model="item.postage" ng-pattern="/^\d+(\.\d{1,2})?$/" required="">
                                    </div>
                                    <div class="form-group form-table-group" ng-show="!$first">
                                        <button type="button" class="btn btn-default" ng-click="remove($index)">
                                            <span class="glyphicon glyphicon-remove"></span>
                                        </button>
                                    </div>
                                    <hr />
                                </div>
                                @endif
                                <div class="form-inline">
                                <div class="form-group"><div class="col-sm-offset-3 col-sm-6">
                                        <button class="btn btn-info" ng-click="add()" type="button">New Item</button>
                                    </div></div>
                                <div class="form-group"><div class="col-sm-offset-3 col-sm-6">
                                        <button class="btn btn-primary btn-lg" ng-click="create()"
                                                ng-disabled="orderForm.$invalid || orderItemForm.$invalid">Create</button>
                                </div></div>
                                </div>
                            </form>

                        </div>
                @if(!Helper::isMobile())
                    </div>
                </div>
                @endif

            </div>
        </div>
    </div>

    <script src="{{ asset('/js/controllers/order.create.js') }}"></script>

@endsection