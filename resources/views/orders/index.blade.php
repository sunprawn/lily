@extends('app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12" ng-app="orderApp" ng-controller="orderCtrl">
                @if(!Helper::isMobile())
                <div class="panel panel-default" >

                    <div class="panel-heading"><h3 class="panel-title">Order List</h3></div>
                    <div class="panel-body">
                @else
                <h3 class="panel-title">Order List</h3><br />
                @endif
                        <div class="row">
                            <div class="col-sm-2">
                                <a class="btn btn-primary btn-lg" href="/order/create">Create new order</a>
                            </div>
                            <div class="col-sm-6">
                                <div ng-show="total.cost" class="form-inline total">
                                    <div class="input-group">
                                        <div class="input-group-addon">Total Cost $</div>
                                        <input type="text" class="form-control" ng-model="total.cost">
                                    </div>
                                    <div class="input-group">
                                        <div class="input-group-addon">Total Profit ¥</div>
                                        <input type="text" class="form-control" ng-model="total.profit">
                                    </div>

                                </div>
                            </div>
                            <div class="col-sm-3 col-sm-offset-1">
                                <div class="form-inline search">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                                        </div>
                                        <input type="text" class="form-control" ng-model="searchText" placeholder="Search" ng-change="addSum()">
                                    </div>

                                </div>
                            </div>
                        </div>

                        <br />

                        <div @if(Helper::isMobile())class="table-responsive"@endif>
                        <table id="importTable"  class="table table-hover table-bordered table-striped">
                            <thead><tr>
                                @if(!Helper::isMobile())
                                <th>
                                    <input type="checkbox" ng-model="selectedAll" ng-click="checkAll()" />
                                </th>
                                @endif
                                <th>
                                    <a href="" ng-click="predicate='tracking';reverse=!reverse">
                                        Tracking #
                                        <span ng-show="predicate=='tracking'">
                                            <span ng-show="!reverse" class="glyphicon glyphicon-sort-by-attributes"></span>
                                            <span ng-show="reverse" class="glyphicon glyphicon-sort-by-attributes-alt"></span>
                                        </span>
                                    </a>
                                </th>
                                <th>
                                    <a href="" ng-click="predicate='customer.name';reverse=!reverse">
                                        Name
                                        <span ng-show="predicate=='customer.name'">
                                            <span ng-show="!reverse" class="glyphicon glyphicon-sort-by-attributes"></span>
                                            <span ng-show="reverse" class="glyphicon glyphicon-sort-by-attributes-alt"></span>
                                        </span>
                                    </a>
                                </th>
                                <th>
                                    <a href="" ng-click="predicate='paid';reverse=!reverse">
                                        Amount (¥)
                                        <span ng-show="predicate=='paid'">
                                            <span ng-show="!reverse" class="glyphicon glyphicon-sort-by-attributes"></span>
                                            <span ng-show="reverse" class="glyphicon glyphicon-sort-by-attributes-alt"></span>
                                        </span>
                                    </a>
                                </th>
                                <th>
                                    <a href="" ng-click="predicate='shipping';reverse=!reverse">
                                        Shipping
                                        <span ng-show="predicate=='shipping'">
                                            <span ng-show="!reverse" class="glyphicon glyphicon-sort-by-attributes"></span>
                                            <span ng-show="reverse" class="glyphicon glyphicon-sort-by-attributes-alt"></span>
                                        </span>
                                    </a>
                                </th>
                                <!--th>
                                    <a href="" ng-click="predicate='profit';reverse=!reverse">
                                        Profit
                                        <span ng-show="predicate=='profit'">
                                            <span ng-show="!reverse" class="glyphicon glyphicon-sort-by-attributes"></span>
                                            <span ng-show="reverse" class="glyphicon glyphicon-sort-by-attributes-alt"></span>
                                        </span>
                                    </a>
                                </th-->
                                <th>
                                    <a href="" ng-click="predicate='dispatch';reverse=!reverse">
                                        Dispatch Date
                                        <span ng-show="predicate=='dispatch'">
                                            <span ng-show="!reverse" class="glyphicon glyphicon-sort-by-attributes"></span>
                                            <span ng-show="reverse" class="glyphicon glyphicon-sort-by-attributes-alt"></span>
                                        </span>
                                    </a>
                                </th>
                                <th>
                                    <a href="" ng-click="predicate='received';reverse=!reverse">
                                        Delivered
                                        <span ng-show="predicate=='received'">
                                            <span ng-show="!reverse" class="glyphicon glyphicon-sort-by-attributes"></span>
                                            <span ng-show="reverse" class="glyphicon glyphicon-sort-by-attributes-alt"></span>
                                        </span>
                                    </a>
                                </th>
                                <th>Event</th>
                                <th>Track</th>
                            </tr></thead>

                            <tbody>
                                <tr ng-repeat="order in data.data | filter:searchText | orderBy:predicate:reverse" ng-class="{delivered: order.received > 0}">
                                    @if(!Helper::isMobile())
                                    <td>
                                        <input type="checkbox" ng-model="order.sum" ng-change="addSum()" />
                                    </td>
                                    @endif

                                    <td>
                                        <a href="" ng-click="loadOrder(order.id)">
                                            [[order.tracking]]
                                        </a>
                                    </td>
                                    <td>[[order.customer.name]]</td>
                                    <td>[[order.paid]]</td>
                                    <td>[[order.shipping]]</td>
                                    <!--td>[[order.profit]]</td-->
                                    <td>[[order.dispatch]]</td>
                                    <td><form>
                                            <input type="checkbox" ng-model="order.received" ng-checked="order.received>0" ng-change="changeDev(order)" />
                                        </form></td>
                                    <td>[[order.last_shipping_log.event]]</td>
                                    <td>
                                        <a class="" href="[[order.courier.url]][[order.tracking]]" target="_blank">
                                            Click Me
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                        <div class="row">
                            <div class="col-sm-5">
                                <div class="dataTables_info" ng-show="data.total" id="retailerTable_info" role="status" aria-live="polite">
                                    Showing [[data.from]] to [[data.to]] of [[data.total]] entries
                                </div>
                            </div>
                            <div class="col-sm-3">
                            </div>
                            <div class="col-sm-4">
                                <div class="dataTables_paginate">
                                    {!! $orders->render() !!}
                                </div>
                            </div>
                        </div>

                        <script type="text/ng-template" id="orderDetail.html">
                            <div class="modal-header">
                                <button type="button" class="close" ng-click="close(orderDetail)">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <h3>Detail of Order #[[orderDetail.id]]</h3>

                            </div>
                            <div class="modal-body">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <form name="orderForm" class="form-horizontal">
                                                <!-- ============ Order detail ============ -->

                                                <div class="form-group">
                                                    <label class="col-sm-5 control-label">Tracking</label>
                                                    <div class="col-sm-5">
                                                        <p class="form-control-static">[[orderDetail.tracking]]</p>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-5 control-label">Shipping Cost</label>
                                                    <div class="col-sm-5">
                                                        <p class="form-control-static">[[orderDetail.shipping]]</p>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-5 control-label">Total Cost</label>
                                                    <div class="col-sm-5">
                                                        <p class="form-control-static">[[orderDetail.cost]]</p>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-5 control-label">Amount Paid</label>
                                                    <div class="col-sm-5">
                                                        <p class="form-control-static">[[orderDetail.paid]]</p>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-5 control-label">Profit</label>
                                                    <div class="col-sm-5">
                                                        <p class="form-control-static">[[orderDetail.profit]]</p>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-5 control-label">Dispatch Date</label>
                                                    <div class="col-sm-5">
                                                        <p class="form-control-static">[[orderDetail.dispatch]]</p>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-5 control-label">Delivered</label>
                                                    <div class="col-sm-5 form-checkbox">
                                                        <input type="checkbox" ng-model="orderDetail.received" ng-checked="orderDetail.received>0" ng-change="changeDev(orderDetail)" />
                                                    </div>
                                                </div>
                                            </form>
                                        </div>

                                        <!-- ============ Customer session ============ -->
                                        <div class="col-md-7">
                                            <form class="form-horizontal">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Customer Name</label>
                                                    <div class="col-sm-8">
                                                        <p class="form-control-static">[[orderDetail.customer.name]]</p>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Phone</label>
                                                    <div class="col-sm-5">
                                                        <p class="form-control-static">[[orderDetail.customer.phone]]</p>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Identity</label>
                                                    <div class="col-sm-8">
                                                        <p class="form-control-static">[[orderDetail.customer.identity]]</p>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Address</label>
                                                    <div class="col-sm-8">
                                                        <p class="form-control-static">[[orderDetail.customer.address]]</p>
                                                    </div>
                                                </div>


                                                <div class="form-group memo">
                                                    <label class="col-sm-4 control-label">Memo</label>
                                                    <div class="col-sm-8">
                                                        <a href="#" ng-click="enableEditor()" ng-hide="editorEnabled">
                                                            <pre>[[orderDetail.memo || 'empty']]</pre>
                                                        </a>
                                                        <span ng-show="editorEnabled">
                                                            <textarea id="memoTextArea" class="form-control" rows="3" ng-model="orderDetail.memo"></textarea>
                                                            <button type="button" class="btn btn-primary" ng-click="saveMemo()">
                                                                <span class="glyphicon glyphicon-ok"></span>
                                                            </button>
                                                            <button type="button" class="btn btn-default" ng-click="disableEditor()">
                                                                <span class="glyphicon glyphicon-remove"></span>
                                                            </button>
                                                        </span>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="row">
                                        <div class="col-md-3 col-md-offset-1">
                                            <form name="courierForm" class="form-horizontal">
                                                <div class="form-group">
                                                    <label class="col-sm-5 control-label">Courier</label>
                                                    <div class="col-sm-7">
                                                        <p class="form-control-static">[[orderDetail.courier.name]]</p>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-5 control-label">Track on</label>
                                                    <div class="col-sm-7">
                                                        <a class="btn btn-primary btn-sm" href="[[orderDetail.courier.url]][[orderDetail.tracking]]" target="_blank">
                                                            Click Me
                                                        </a>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-md-8">
                                            <table class="table-striped table table-bordered" ng-show="orderDetail.shipping_logs.length > 0">
                                                <thead>
                                                <th>日期</th>
                                                <th>地点</th>
                                                <th>事件</th>
                                                </thead>
                                                <tbody>
                                                <tr ng-repeat="log in orderDetail.shipping_logs">
                                                    <td>[[log.time]]</td>
                                                    <td>[[log.location]]</td>
                                                    <td>[[log.event]]</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" >


                                </div>
                                <hr />
                                <!-- ============ Order item session ============ -->
                                <div class="row">

                                    <form>
                                        <table class="table table-bordered table-striped">
                                            <thead>
                                            <th>Barcode</th>
                                            <th>Brand</th>
                                            <th>Name</th>
                                            <th>Quantity</th>
                                            <th>Price</th>
                                            <th>Postage</th>
                                            </thead>
                                            <tbody>

                                            <tr ng-repeat="item in orderDetail.order_items">
                                                <td>[[item.product.barcode]]</td>

                                                <td>[[item.product.brand.name]]</td>
                                                <td>[[item.product.name]]</td>
                                                <td>[[item.quantity]]</td>
                                                <td>[[item.price]]</td>
                                                <td>[[item.postage]]</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </form>
                                </div>

                            </div>
                        </script>
                @if(!Helper::isMobile())
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>



    <script src="{{ asset('/js/controllers/order.index.js') }}"></script>

@endsection