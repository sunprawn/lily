@extends('app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1" ng-app="orderApp" ng-controller="orderCtrl">
                @if(!Helper::isMobile())
                <div class="panel panel-default">
                    <div class="panel-heading"><h3 class="panel-title">Detail of Order #[[orderDetail.id]]</h3></div>
                    <div class="panel-body">
                @else
                    <h3 class="panel-title">Detail of Order #[[orderDetail.id]]</h3><br />
                @endif
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-5">
                                    <form name="orderForm" class="form-horizontal">
                                        <!-- ============ Order detail ============ -->

                                        <div class="form-group">
                                            <label class="col-sm-5 control-label">Tracking</label>
                                            <div class="col-sm-5">
                                                <p class="form-control-static">[[orderDetail.tracking]]</p>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-5 control-label">Shipping Cost</label>
                                            <div class="col-sm-5">
                                                <p class="form-control-static">[[orderDetail.shipping]]</p>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-5 control-label">Total Cost</label>
                                            <div class="col-sm-5">
                                                <p class="form-control-static">[[orderDetail.cost]]</p>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-5 control-label">Amount Paid</label>
                                            <div class="col-sm-5">
                                                <p class="form-control-static">[[orderDetail.paid]]</p>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-5 control-label">Profit</label>
                                            <div class="col-sm-5">
                                                <p class="form-control-static">[[orderDetail.profit]]</p>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-5 control-label">Dispatch Date</label>
                                            <div class="col-sm-5">
                                                <p class="form-control-static">[[orderDetail.dispatch]]</p>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-5 control-label">Delivered</label>
                                            <div class="col-sm-5 form-checkbox">
                                                <input type="checkbox" ng-model="orderDetail.received" ng-checked="orderDetail.received>0" ng-change="changeDev(orderDetail)" />
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <!-- ============ Customer session ============ -->
                                <div class="col-md-7">
                                    <form class="form-horizontal">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Customer Name</label>
                                            <div class="col-sm-8">
                                                <p class="form-control-static">[[orderDetail.customer.name]]</p>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Phone</label>
                                            <div class="col-sm-5">
                                                <p class="form-control-static">[[orderDetail.customer.phone]]</p>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Identity</label>
                                            <div class="col-sm-8">
                                                <p class="form-control-static">[[orderDetail.customer.identity]]</p>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Address</label>
                                            <div class="col-sm-8">
                                                <p class="form-control-static">[[orderDetail.customer.address]]</p>
                                            </div>
                                        </div>
                                        <div class="form-group memo">
                                            <label class="col-sm-4 control-label">Memo</label>
                                            <div class="col-sm-8">
                                                <a ng-click="enableEditor()" ng-hide="editorEnabled">
                                                    <pre>[[orderDetail.memo || 'empty']]</pre>
                                                </a>
                                                <span ng-show="editorEnabled">
                                                    <textarea id="memoTextArea" class="form-control" rows="3" ng-model="orderDetail.memo"></textarea>
                                                    <button type="button" class="btn btn-primary" ng-click="saveMemo()">
                                                        <span class="glyphicon glyphicon-ok"></span>
                                                    </button>
                                                    <button type="button" class="btn btn-default" ng-click="disableEditor()">
                                                        <span class="glyphicon glyphicon-remove"></span>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <hr />
                            <div class="row">
                                <div class="col-md-3 col-md-offset-1">
                                    <form name="courierForm" class="form-horizontal">
                                        <div class="form-group">
                                            <label class="col-sm-5 control-label">Courier</label>
                                            <div class="col-sm-6">
                                                <p class="form-control-static">[[orderDetail.courier.name]]</p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-5 control-label">Track on</label>
                                            <div class="col-sm-6">
                                                <a class="btn btn-primary btn-sm" href="[[orderDetail.courier.url]][[orderDetail.tracking]]" target="_blank">
                                                    Click Me
                                                </a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-md-8">
                                    <div @if(Helper::isMobile())class="table-responsive"@endif>
                                    <table class="table-striped table table-bordered" ng-show="orderDetail.shipping_logs.length > 0">
                                        <thead>
                                        <th>日期</th>
                                        <th>地点</th>
                                        <th>事件</th>
                                        </thead>
                                        <tbody>
                                        <tr ng-repeat="log in orderDetail.shipping_logs">
                                            <td>[[log.time]]</td>
                                            <td>[[log.location]]</td>
                                            <td>[[log.event]]</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr />

                        <!-- ============ Order item session ============ -->
                        <div class="row">

                            <form>
                                <div @if(Helper::isMobile())class="table-responsive"@endif>
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    <th>Barcode</th>
                                    <th>Brand</th>
                                    <th>Name</th>
                                    <th>Quantity</th>
                                    <th>Price</th>
                                    <th>Postage</th>
                                    </thead>
                                    <tbody>

                                    <tr ng-repeat="item in orderDetail.order_items">
                                        <td>[[item.product.barcode]]</td>

                                        <td>[[item.product.brand.name]]</td>
                                        <td>[[item.product.name]]</td>
                                        <td>[[item.quantity]]</td>
                                        <td>[[item.price]]</td>
                                        <td>[[item.postage]]</td>
                                    </tr>
                                    </tbody>
                                </table>
                                </div>
                            </form>
                        </div>
                @if(!Helper::isMobile())
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>

    <script type="application/javascript">
        orderDetail = {!! $order !!};
    </script>

    <script src="{{ asset('/js/controllers/order.show.js') }}"></script>

@endsection