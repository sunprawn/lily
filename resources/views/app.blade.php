<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<title>Lily</title>

	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet">
    <!--link href="{{ asset('/css/dataTables.bootstrap.css') }}" rel="stylesheet"-->

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
    <script src="{{ asset('/js/jquery.min.js') }}"></script>
    <script src="{{ asset('/js/bootstrap.min.js') }}"></script>
    <!--script src="{{ asset('/js/dataTables.min.js') }}"></script-->
    <!--script src="{{ asset('/js/dataTables.bootstrap.js') }}"></script-->
    <script src="{{ asset('/js/angular.min.js') }}"></script>
    <script src="{{ asset('/js/ui-bootstrap.min.js') }}"></script>
</head>
<body>
	<nav class="navbar navbar-default navbar-inverse">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">Laravel</a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
                    @if (Auth::guest())
					    <li><a href="{{ url('/') }}">Home</a></li>
                    @else
                        <li class="{{ Request::is('product*')?'active':'' }}">
                            <a href="{{ url('/product') }}">Product</a>
                        </li>
                        <li class="{{ Request::is('import*')?'active':'' }}">
                            <a href="{{ url('/import') }}">Import</a>
                        </li>
                        <li class="{{ Request::is('order*')?'active':'' }}">
                            <a href="{{ url('/order') }}">Order</a>
                        </li>
                        <li class="{{ Request::is('customer*')?'active':'' }}">
                            <a href="{{ url('/customer') }}">Customer</a>
                        </li>
                        <li class="{{ Request::is('retailer*')?'active':'' }}">
                            <a href="{{ url('/retailer') }}">Retailer</a>
                        </li>
                        <li class="{{ Request::is('brand*')?'active':'' }}">
                            <a href="{{ url('/brand') }}">Brand</a>
                        </li>
                        <li class="{{ Request::is('courier*')?'active':'' }}">
                            <a href="{{ url('/courier') }}">Courier</a>
                        </li>
                    @endif
				</ul>

				<ul class="nav navbar-nav navbar-right">
					@if (Auth::guest())
						<li><a href="{{ url('/auth/login') }}">Login</a></li>
						<!--li><a href="{{ url('/auth/register') }}">Register</a></li-->
					@else
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="{{ url('/auth/logout') }}">Logout</a></li>
							</ul>
						</li>
					@endif
				</ul>
			</div>
		</div>
	</nav>

	@yield('content')

	<!-- Scripts -->

</body>
</html>
