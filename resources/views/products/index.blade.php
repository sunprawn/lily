@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1" ng-app="productApp" ng-controller="productCtrl">
                @if(!Helper::isMobile())
                <div class="panel panel-default">
                    <div class="panel-heading"><h3 class="panel-title">Product List</h3></div>
                    <div class="panel-body">
                @else
                <h3 class="panel-title">Product List</h3><br />
                @endif
                        <div @if(Helper::isMobile())class="table-responsive"@endif>
                        <table id="productTable" class="table table-hover table-bordered table-striped">
                            <thead><tr>
                                <th>
                                    <a href="" ng-click="predicate='barcode';reverse=!reverse">
                                        Barcode
                                        <span ng-show="predicate=='barcode'">
                                            <span ng-show="!reverse" class="glyphicon glyphicon-sort-by-attributes"></span>
                                            <span ng-show="reverse" class="glyphicon glyphicon-sort-by-attributes-alt"></span>
                                        </span>
                                    </a>
                                </th>
                                <th>
                                    <a href="" ng-click="predicate='brand.name';reverse=!reverse">
                                        Brand
                                        <span ng-show="predicate=='brand.name'">
                                            <span ng-show="!reverse" class="glyphicon glyphicon-sort-by-attributes"></span>
                                            <span ng-show="reverse" class="glyphicon glyphicon-sort-by-attributes-alt"></span>
                                        </span>
                                    </a>
                                </th>
                                <th>
                                    <a href="" ng-click="predicate='name';reverse=!reverse">
                                        Name
                                        <span ng-show="predicate=='name'">
                                            <span ng-show="!reverse" class="glyphicon glyphicon-sort-by-attributes"></span>
                                            <span ng-show="reverse" class="glyphicon glyphicon-sort-by-attributes-alt"></span>
                                        </span>
                                    </a>
                                </th>
                                <th>
                                    <a href="" ng-click="predicate='size';reverse=!reverse">
                                        Size
                                        <span ng-show="predicate=='size'">
                                            <span ng-show="!reverse" class="glyphicon glyphicon-sort-by-attributes"></span>
                                            <span ng-show="reverse" class="glyphicon glyphicon-sort-by-attributes-alt"></span>
                                        </span>
                                    </a>
                                </th>
                                <th>
                                    <a href="" ng-click="predicate='stock';reverse=!reverse">
                                        Stock
                                        <span ng-show="predicate=='stock'">
                                            <span ng-show="!reverse" class="glyphicon glyphicon-sort-by-attributes"></span>
                                            <span ng-show="reverse" class="glyphicon glyphicon-sort-by-attributes-alt"></span>
                                        </span>
                                    </a>
                                </th>
                            </tr></thead>

                            <tbody>
                            <tr ng-repeat="product in data.data | orderBy:predicate:reverse">
                                <td>[[product.barcode]]</td>
                                <td>[[product.brand.name]]</td>
                                <td>[[product.name]]</td>
                                <td>[[product.size]]</td>
                                <td>[[product.stock]]</td>
                            </tr>
                            </tbody>
                        </table>
                        </div>
                @if(!Helper::isMobile())
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>

    <script src="{{ asset('/js/controllers/product.index.js') }}"></script>
@endsection