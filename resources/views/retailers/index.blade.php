@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                @if(!Helper::isMobile())
                <div class="panel panel-default">
                    <div class="panel-heading"><h3 class="panel-title">Retailer List</h3></div>
                    <div class="panel-body">
                @else
                <h3 class="panel-title">Retailer List</h3><br />
                @endif
                        <a class="btn btn-primary btn-lg" href="/retailer/create">Create new Retailer</a><br /><br />
                        <table id="retailerTable" class="table table-hover table-bordered">
                            <thead><tr>
                                <th>ID</th>
                                <th>Name</th>
                                <!--th>Sort Order</th-->
                            </tr></thead>
                            @foreach($retailers as $retailer)
                                <tr>
                                    <td>{{ $retailer->id }}</td>
                                    <td>{{ $retailer->name }}</td>
                                    <!--td>{{ $retailer->sort }}</td-->
                                </tr>

                            @endforeach
                        </table>
                        <div class="row">
                            <div class="col-sm-8"></div>
                            <div class="col-sm-4">
                                <div class="dataTables_paginate">
                                    {!! $retailers->render() !!}
                                </div>
                            </div>
                        </div>
                @if(!Helper::isMobile())
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>

@endsection