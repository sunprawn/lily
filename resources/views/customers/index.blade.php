@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                @if(!Helper::isMobile())
                <div class="panel panel-default">
                    <div class="panel-heading"><h3 class="panel-title">Customer List</h3></div>
                    <div class="panel-body">
                @else
                    <h3 class="panel-title">Customer List</h3><br />
                @endif
                        <div @if(Helper::isMobile())class="table-responsive"@endif>
                        <table id="customerTable" class="table table-hover table-bordered table-striped">
                            <thead><tr>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Address</th>
                                <th>身份证</th>
                            </tr></thead>
                            @foreach($customers as $customer)
                                <tr>
                                    <td>{{ $customer->name }}</td>
                                    <td>{{ $customer->phone }}</td>
                                    <td>{{ $customer->address }}</td>
                                    <td>{{ $customer->identity }}</td>
                                </tr>
                            @endforeach
                        </table>
                        </div
                @if(!Helper::isMobile())
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>

@endsection