@extends('app')


@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                @if(!Helper::isMobile())
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title">Create Brand Record</h3></div>

                    <div class="panel-body">
                @else
                <h3 class="panel-title">Create Brand Record</h3><br />
                @endif
                        <div>
                            <form name="importForm" method="post" action="/brand" class="form-horizontal">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <div class="form-group">
                                    <label for="inputName" class="col-sm-3 control-label">Brand Name</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" id="inputName" name="name" placeholder="Brand Name" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputDescription" class="col-sm-3 control-label">Description</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" id="inputDescription" name="description" placeholder="Description" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputSort" class="col-sm-3 control-label">Sort Order</label>
                                    <div class="col-sm-5">
                                        <input type="number" class="form-control" id="inputSort" name="sort" placeholder="Sort 1 - 99" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-6">
                                        <button class="btn btn-primary btn-lg" type="submit">Create</button>
                                    </div>
                                </div>
                            </form>

                        </div>
                @if(!Helper::isMobile())
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>


@endsection