@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                @if(!Helper::isMobile())
                <div class="panel panel-default">
                    <div class="panel-heading"><h3 class="panel-title">Brand List</h3></div>
                    <div class="panel-body">
                @else
                <h3 class="panel-title">Brand List</h3><br />
                @endif
                        <a class="btn btn-primary btn-lg" href="/brand/create">Create new Brand</a><br /><br />

                        <table id="brandTable" class="table table-hover table-bordered">
                            <thead><tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Description</th>
                            </tr></thead>
                            @foreach($brands as $brand)
                                <tr>
                                    <td>{{ $brand->id }}</td>
                                    <td>{{ $brand->name }}</td>
                                    <td>{{ $brand->description }}</td>
                                </tr>

                            @endforeach
                        </table>
                @if(!Helper::isMobile())
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>

@endsection