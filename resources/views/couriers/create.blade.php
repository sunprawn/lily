@extends('app')


@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                @if(!Helper::isMobile())
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title">Create Courier Record</h3></div>

                    <div class="panel-body">
                @else
                    <h3 class="panel-title">Create Courier Record</h3>
                @endif
                        <div>
                            <form name="importForm" method="post" action="/courier" class="form-horizontal">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <div class="form-group">
                                    <label for="inputName" class="col-sm-3 control-label">Courier Name</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" id="inputName" name="name" placeholder="Courier Name" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputWebsite" class="col-sm-3 control-label">Website</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" id="inputWebsite" name="website" placeholder="Website" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputURL" class="col-sm-3 control-label">Tracking URL</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="inputURL" name="url" placeholder="Tracking URL" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputSort" class="col-sm-3 control-label">Sort Order</label>
                                    <div class="col-sm-5">
                                        <input type="number" class="form-control" id="inputSort" name="sort" placeholder="Sort 1 - 99" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-6">
                                        <button class="btn btn-primary btn-lg" type="submit">Create</button>
                                    </div>
                                </div>
                            </form>

                        </div>
                @if(!Helper::isMobile())
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>


@endsection