@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                @if(!Helper::isMobile())
                <div class="panel panel-default">
                    <div class="panel-heading"><h3 class="panel-title">Couriers List</h3></div>
                    <div class="panel-body">
                @else
                    <h3 class="panel-title">Couriers List</h3><br />
                @endif
                        <a class="btn btn-primary btn-lg" href="/courier/create">Create new Courier</a><br /><br />
                        <div @if(Helper::isMobile())class="table-responsive"@endif>
                        <table id="customerTable" class="table table-hover table-bordered table-striped">
                            <thead><tr>
                                <th>Name</th>
                                <th>Website</th>
                                <th>URL</th>
                                <!--th>Sort</th-->
                            </tr></thead>
                            @foreach($couriers as $courier)
                                <tr>
                                    <td>{{ $courier->name }}</td>
                                    <td>{{ $courier->website }}</td>
                                    <td>{{ $courier->url }}</td>
                                    <!--td>{{ $courier->sort }}</td-->
                                </tr>
                            @endforeach
                        </table>
                        </div>
                @if(!Helper::isMobile())
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>

@endsection