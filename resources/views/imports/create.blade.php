@extends('app')


@section('content')
    <script src="{{ asset('/js/jquery-ui.min.js') }}"></script>
    <link href="{{ asset('/css/jquery-ui.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/jquery-ui.theme.min.css') }}" rel="stylesheet">

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @if(!Helper::isMobile())
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title">Create Shopping List</h3></div>

                    <div class="panel-body">
                @else
                <h3 class="panel-title">Create Shopping List</h3><br />
                @endif
                        <div ng-app="importApp" ng-controller="formCtrl">
                            <form name="importForm" ng-submit="submitForm(importForm.$valid)" class="form-horizontal" novalidate>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <div class="form-group">
                                    <label for="inputDate" class="col-sm-3 control-label">Retailer</label>
                                    <div class="col-sm-5">
                                        <select class="form-control" ng-model="import.retailer" required="">
                                            @foreach( $retailers as $retailer)
                                                <option value="{{ $retailer->id }}">{{ $retailer->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <label class="col-sm-4 control-label" style="color:red;text-align:left;"
                                           ng-show="importForm.retailer.$invalid && importForm.retailer.$dirty">
                                        Retailer is required
                                    </label>
                                </div>

                                <div class="form-group">
                                    <label for="inputDate" class="col-sm-3 control-label">Purchased Date</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" id="inputDate" name="purchased" placeholder="Purchased Date"
                                               ng-model="import.purchased" required>
                                    </div>
                                    <label class="col-sm-4 control-label" style="color:red;text-align:left;"
                                           ng-show="importForm.purchased.$invalid && importForm.purchased.$dirty">
                                        Dispatch date is required
                                    </label>
                                </div>

                                <hr />

                            </form>

                            <form name="importItemForm" ng-submit="submitForm(importItemForm.$valid)" novalidate>
                                @if(!Helper::isMobile())
                                <table class="table">
                                    <thead>
                                        <th>Barcode</th>
                                        <th>Brand</th>
                                        <th>Name</th>
                                        <th>Size</th>
                                        <th>Price</th>
                                        <th>Quantity</th>
                                        <th></th>
                                    </thead>
                                    <tbody>

                                        <tr ng-repeat="item in import.items">
                                            <td>
                                                <div class="form-group form-table-group">
                                                    <label for="inputBarcode" class="sr-only">Product Barcode</label>
                                                    <input type="text" class="form-control" id="inputBarcode" name="barcode[]" placeholder="Product Barcode"
                                                           ng-model="item.barcode" ng-blur="getProduct($index)">
                                                </div>
                                            </td>

                                            <td>
                                                <div class="form-group form-table-group">
                                                    <label for="inputBrand" class="sr-only">Product Barcode</label>
                                                    <select class="form-control" ng-model="item.brand" required="">
                                                        @foreach( $brands as $brand)
                                                            <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </td>

                                            <td>
                                                <div class="form-group form-table-group">
                                                    <label for="inputName" class="sr-only">Product Name</label>
                                                    <input type="text" class="form-control" id="inputName" name="name[]" placeholder="Product Name"
                                                           ng-model="item.name">
                                                </div>
                                            </td>

                                            <td>
                                                <div class="form-group form-table-group">
                                                    <label for="inputSize" class="sr-only">Product Size</label>
                                                    <div style="width:95px">
                                                    <input type="text" class="form-control" id="inputSize" name="size[]" placeholder="Size"
                                                           ng-model="item.size" required="">
                                                    </div>
                                                </div>
                                            </td>

                                            <td>
                                                <div class="form-group form-table-group">
                                                    <label for="inputPrice" class="sr-only">Price</label>
                                                    <div style="width:95px">
                                                    <input type="text" class="form-control" id="inputPrice" name="price[]" placeholder="Unit Price"
                                                           ng-model="item.price" ng-pattern="/^\d+(\.\d{1,2})?$/" required="">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group form-table-group">
                                                    <label for="inputQuantity" class="sr-only">Quantity</label>
                                                    <div style="width:85px">
                                                    <input type="text" class="form-control" id="inputQuantity" name="quantity[]" placeholder="Quantity"
                                                           ng-model="item.quantity" ng-pattern="/^\d+$/" required="">
                                                    </div>
                                                </div>
                                            </td>

                                            <td>
                                                <div class="form-group form-table-group" ng-show="!$first">
                                                    <button type="button" class="btn btn-default" ng-click="remove($index)">
                                                        <span class="glyphicon glyphicon-remove"></span>
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                                @else
                                <div ng-repeat="item in import.items">
                                    <div class="form-group form-table-group">
                                        <label for="inputBarcode" class="control-label">Product Barcode</label>
                                        <input type="text" class="form-control" id="inputBarcode" name="barcode[]" placeholder="Product Barcode"
                                               ng-model="item.barcode" ng-blur="getProduct($index)">
                                    </div>
                                    <div class="form-group form-table-group" ng-show="item.type == 'new' && item.barcode != ''">
                                        <label for="inputBrand" class="control-label">Brand</label>
                                        <select class="form-control" ng-model="item.brand" required="">
                                            @foreach( $brands as $brand)
                                                <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group form-table-group">
                                        <label for="inputName" class="control-label">Product Name</label>
                                        <input type="text" class="form-control" id="inputName" name="name[]" placeholder="Product Name"
                                               ng-model="item.name">
                                    </div>
                                    <div class="form-group form-table-group" ng-show="item.type == 'new' && item.barcode != ''">
                                        <label for="inputSize" class="control-label">Product Size</label>
                                        <input type="text" class="form-control" id="inputSize" name="size[]" placeholder="Size"
                                               ng-model="item.size" required="">
                                    </div>
                                    <div class="form-group form-table-group">
                                        <label for="inputPrice" class="control-label">Price</label>
                                        <input type="text" class="form-control" id="inputPrice" name="price[]" placeholder="Unit Price"
                                               ng-model="item.price" ng-pattern="/^\d+(\.\d{1,2})?$/" required="">
                                    </div>
                                    <div class="form-group form-table-group">
                                        <label for="inputQuantity" class="control-label">Quantity</label>
                                        <input type="text" class="form-control" id="inputQuantity" name="quantity[]" placeholder="Quantity"
                                               ng-model="item.quantity" ng-pattern="/^\d+$/" required="">
                                    </div>
                                    <div class="form-group form-table-group" ng-show="!$first">
                                        <button type="button" class="btn btn-default" ng-click="remove($index)">
                                            <span class="glyphicon glyphicon-remove"></span>
                                        </button>
                                    </div>
                                    <hr />
                                </div>
                                @endif
                                <div class="form-inline">
                                <div class="form-group"><div class="col-sm-offset-3 col-sm-6">
                                        <button class="btn btn-info" ng-click="add()" type="button">New Item</button>
                                    </div></div>
                                <div class="form-group"><div class="col-sm-offset-3 col-sm-6">
                                        <button class="btn btn-primary btn-lg" ng-click="create()"
                                                ng-disabled="importForm.$invalid || importItemForm.$invalid">Create</button>
                                </div></div>
                                </div>
                            </form>

                        </div>
                @if(!Helper::isMobile())
                    </div>
                </div>
                @endif

            </div>
        </div>
    </div>

    <script src="{{ asset('/js/controllers/import.create.js') }}"></script>

@endsection