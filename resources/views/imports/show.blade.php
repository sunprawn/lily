@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @if(!Helper::isMobile())
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title">Shopping List {{ $import?$import->id:'' }}</h3></div>
                    <div class="panel-body">
                @else
                <h3 class="panel-title">Shopping List {{ $import?$import->id:'' }}</h3><br />
                @endif
                        @if($import)
                        <form class="form-horizontal">

                            <div class="form-group">
                                <label for="inputDate" class="col-sm-3 control-label">Retailer</label>
                                <div class="col-sm-5">
                                    <p class="form-control-static">{{ $import->retailer->name }}</p>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputDate" class="col-sm-3 control-label">Purchased Date</label>
                                <div class="col-sm-5">
                                    <p class="form-control-static">{{ $import->purchase_date }}</p>
                                </div>
                            </div>

                        </form>
                        <hr />
                        <div @if(Helper::isMobile())class="table-responsive"@endif>
                        <table id="importTable" class="table table-hover table-bordered table-striped">
                            <thead><tr>
                                <th>Barcode</th>
                                <th>Brand</th>
                                <th>Name</th>
                                <th>Size</th>
                                <th>Price</th>
                                <th>Quantity</th>
                                <th>Quantity Left</th>
                            </tr></thead>
                            @foreach($import->importItems as $item)
                                <tr>
                                    <td>{{ $item->product->barcode }}</td>
                                    <td>{{ $item->product->brand->name }}</td>
                                    <td>{{ $item->product->name }}</td>
                                    <td>{{ $item->product->size }}</td>
                                    <td>${{ $item->price }}</td>
                                    <td>{{ $item->quantity }}</td>
                                    <td>{{ $item->left }}</td>
                                </tr>

                            @endforeach
                        </table>
                        </div>
                        @else
                        <div class="alert alert-warning">No record found</div>
                        @endif
                @if(!Helper::isMobile())
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>

@endsection