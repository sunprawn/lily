@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12" ng-app="importApp" ng-controller="importCtrl">
                @if(!Helper::isMobile())
                <div class="panel panel-default">
                    <div class="panel-heading"><h3 class="panel-title">Shopping List</h3></div>
                    <div class="panel-body">
                @else
                <h3 class="panel-title">Shopping List</h3><br />
                @endif
                        <a class="btn btn-primary btn-lg" href="/import/create">Create new import</a><br /><br />
                        <div @if(Helper::isMobile())class="table-responsive"@endif>
                        <table id="importTable" class="table table-hover table-bordered table-striped">
                            <thead><tr>
                                <th>
                                    <a href="" ng-click="predicate='import_id';reverse=!reverse">
                                        Batch ID
                                        <span ng-show="predicate=='import_id'">
                                            <span ng-show="!reverse" class="glyphicon glyphicon-sort-by-attributes"></span>
                                            <span ng-show="reverse" class="glyphicon glyphicon-sort-by-attributes-alt"></span>
                                        </span>
                                    </a>
                                </th>
                                <th>
                                    <a href="" ng-click="predicate='product.barcode';reverse=!reverse">
                                        Barcode
                                        <span ng-show="predicate=='product.barcode'">
                                            <span ng-show="!reverse" class="glyphicon glyphicon-sort-by-attributes"></span>
                                            <span ng-show="reverse" class="glyphicon glyphicon-sort-by-attributes-alt"></span>
                                        </span>
                                    </a>
                                </th>
                                <th>
                                    <a href="" ng-click="predicate='product.brand.name';reverse=!reverse">
                                        Brand
                                        <span ng-show="predicate=='product.brand.name'">
                                            <span ng-show="!reverse" class="glyphicon glyphicon-sort-by-attributes"></span>
                                            <span ng-show="reverse" class="glyphicon glyphicon-sort-by-attributes-alt"></span>
                                        </span>
                                    </a>
                                </th>
                                <th>
                                    <a href="" ng-click="predicate='product.name';reverse=!reverse">
                                        Name
                                        <span ng-show="predicate=='product.name'">
                                            <span ng-show="!reverse" class="glyphicon glyphicon-sort-by-attributes"></span>
                                            <span ng-show="reverse" class="glyphicon glyphicon-sort-by-attributes-alt"></span>
                                        </span>
                                    </a>
                                </th>
                                <th>
                                    <a href="" ng-click="predicate='price';reverse=!reverse">
                                        Price
                                        <span ng-show="predicate=='price'">
                                            <span ng-show="!reverse" class="glyphicon glyphicon-sort-by-attributes"></span>
                                            <span ng-show="reverse" class="glyphicon glyphicon-sort-by-attributes-alt"></span>
                                        </span>
                                    </a>
                                </th>
                                <th>
                                    <a href="" ng-click="predicate='quantity';reverse=!reverse">
                                        Quantity
                                        <span ng-show="predicate=='quantity'">
                                            <span ng-show="!reverse" class="glyphicon glyphicon-sort-by-attributes"></span>
                                            <span ng-show="reverse" class="glyphicon glyphicon-sort-by-attributes-alt"></span>
                                        </span>
                                    </a>
                                </th>
                                <th>
                                    <a href="" ng-click="predicate='left';reverse=!reverse">
                                        Quantity Left
                                        <span ng-show="predicate=='left'">
                                            <span ng-show="!reverse" class="glyphicon glyphicon-sort-by-attributes"></span>
                                            <span ng-show="reverse" class="glyphicon glyphicon-sort-by-attributes-alt"></span>
                                        </span>
                                    </a>
                                </th>
                            </tr></thead>

                            <tbody>
                            <tr ng-repeat="item in data.data | orderBy:predicate:reverse" ng-class="{runout: item.left <= 0}">
                                <td><a href="/import/[[item.import_id]]">[[item.import_id]]</a></td>
                                <td>[[item.product.barcode]]</td>
                                <td>[[item.product.brand.name]]</td>
                                <td>[[item.product.name]]</td>
                                <td>[[item.price]]</td>
                                <td>[[item.quantity]]</td>
                                <td>[[item.left]]</td>
                            </tr>
                            </tbody>
                        </table>
                        </div>
                        <div class="row">
                            <div class="col-sm-8"></div>
                            <div class="col-sm-4">
                                <div class="dataTables_paginate">
                                    {!! $items->render() !!}
                                </div>
                            </div>
                        </div>
                @if(!Helper::isMobile())
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>

    <script src="{{ asset('/js/controllers/import.index.js') }}"></script>

@endsection