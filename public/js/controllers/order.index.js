var app = angular.module('orderApp', ['ui.bootstrap'], function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
});

app.controller('orderCtrl', function($scope, $http, $modal, filterFilter) {
    $scope.data = {};
    $scope.total = {};
    $scope.orderDetail = {};
    $scope.editorEnabled = false;

    var _isMobile = (function() {
        var check = false;
        (function(a){
            if(/(android|bb\d+|meego).+mobile|iphone|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a))
                check = true
        })(navigator.userAgent||navigator.vendor||window.opera);
        return check;
    })();

    $scope.getOrders = function() {
        $http.get('/api/order/' + window.location.search)
            .success(function(respond){
                $scope.data = respond;
            })
            .error(function() {

            });
    };

    $scope.getOrders();

    $scope.changeDev = function(order) {
        var orderId = order.id;
        var data = {};
        //data.received = $scope.data.data[$scope.data.data.indexOf(order)].received;
        data.received = order.received;

        $http.put('/order/' + orderId, data)
            .success(function(respond) {
                if (respond.error == '') {

                } else {
                    alert(respond.error);
                }
            });

    };

    $scope.checkAll = function() {
        if ($scope.selectedAll) {
            $scope.selectedAll = true;
        } else {
            $scope.selectedAll = false;
        }

        angular.forEach($scope.data.data, function (item) {
            item.sum = $scope.selectedAll;
        });

        $scope.addSum();
    };

    $scope.addSum = function() {
        $scope.total.cost = 0;
        $scope.total.profit = 0;

        angular.forEach(filterFilter($scope.data.data,$scope.searchText), function (item) {

            if (item.sum) {
                $scope.total.cost += parseFloat(item.cost);
                $scope.total.profit += parseFloat(item.profit);
            }
        });

        $scope.total.cost = $scope.total.cost?$scope.total.cost.toFixed(2):$scope.total.cost;
        $scope.total.profit = $scope.total.profit?$scope.total.profit.toFixed(2):$scope.total.profit;

    };

    $scope.loadOrder = function(id) {
        if (_isMobile) {
            window.location.href = "/order/" + id;
        }

        $scope.editorEnabled = false;

        $http.get('/order/' + id + "?type=json")
            .success(function(respond) {
                if(respond != undefined) {
                    $scope.orderDetail = respond;
                } else {
                    $scope.orderDetail = {};
                }
            })
            .error(function() {

            });

        var modalInstance = $modal.open({
            templateUrl: 'orderDetail.html',
            //controller: 'orderCtrl',
            scope: $scope,
            size: 'lg'
        });

        $scope.close = function() {
            modalInstance.dismiss();
            $scope.getOrders();
        }
    };

    $scope.enableEditor = function() {
        $scope.editorEnabled = true;
        $scope.editableMemo = $scope.orderDetail.memo;

    };
    $scope.disableEditor = function() {
        $scope.orderDetail.memo = $scope.editableMemo;
        $scope.editorEnabled = false;
    };
    $scope.saveMemo = function() {
        var data = {};

        data.memo = $scope.orderDetail.memo;

        $http.put('/order/' + $scope.orderDetail.id, data)
            .success(function(respond) {
                if (respond.error == '') {

                } else {
                    alert(respond.error);
                }
                $scope.editorEnabled = false;
            })
            .error(function() {

            });


    };
});

/*
$(document).ready(function(){
    $('#importTable').DataTable({
        "paging": false,
        "info"  :false,
        "aaSorting": []
    });
});*/
