var app = angular.module('orderApp', [], function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
});

app.controller('formCtrl', function($scope, $http) {
    $scope.order = {};
    $scope.order.customer = {};
    $scope.order.items = [];

    $scope.order.items.push({
        barcode:    "",
        name:       "",
        brand:      "",
        price:      "",
        quantity:   "",
        postage:    "0.00"
    });

    $scope.add = function() {
        $scope.order.items.push({
            barcode:    "",
            name:       "",
            brand:      "",
            price:      "",
            quantity:   "",
            postage:    "0.00"
        });
    };

    $scope.remove = function(index) {
        $scope.order.items.splice(index, 1);
    }

    /**
     * get the product detail
     * @param index
     */
    $scope.getProduct = function(index) {
        var barcode = $scope.order.items[index].barcode;

        if(barcode == ''){
            return;
        }

        $http.get('/product/bar/' + barcode)
            .success(function(respond) {
                if (respond.error == '') {
                    $scope.order.items[index].name = respond.name;

                    $scope.order.items[index].brand = respond.brandName;

                }
            })
            .error(function() {

            });
    };

    /**
     * get customer detail
     */
    $scope.getCustomer = function() {
        var name = $scope.order.customer.name;
        var identity = $scope.order.customer.identity;

        if(name == '' || identity == '') {
            return;
        }

        $http.get('/customer/get/' + name + '/' + identity)
            .success(function(respond) {
                if (respond.error == '') {
                    $scope.order.customer.address = respond.address;
                    $scope.order.customer.phone = respond.phone;
                }
            })
            .error(function() {

            });
    };

    /**
     * submit the form
     */
    $scope.create = function() {

        $http.post('/order', $scope.order)
            .success(function(respond) {
                if(respond.error == '') {
                    window.location = "/order/" + respond.orderID;
                } else {
                    alert(respond.error);
                }
            })
            .error(function() {
                alert('Internal error on creating shopping, please contact admin');
            });
    };
});

$(document).ready(function(){
    $( "#inputDate" ).datepicker({
        dateFormat: "yy-mm-dd"
    });
});