var app = angular.module('importApp', [], function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
});

app.controller('formCtrl', function($scope, $http, $location) {
    $scope.import = {};
    $scope.import.items = [];

    $scope.import.items.push({
        barcode:    "",
        name:       "",
        size:       "",
        brand:      "",
        price:      "",
        quantity:   "",
        type:      "new"
    });

    $scope.add = function() {
        $scope.import.items.push({
            barcode:    "",
            name:       "",
            size:       "",
            brand:      "",
            price:      "",
            quantity:   "",
            type:      "new"
        });
    };

    $scope.remove = function(index) {
        $scope.import.items.splice(index, 1);
    }

    /**
     * get the product detail
     * @param index
     */
    $scope.getProduct = function(index) {
        var barcode = $scope.import.items[index].barcode;

        if(barcode == ''){
            return;
        }

        $http.get('/product/bar/' + barcode)
            .success(function(respond) {
                if (respond.error == '') {
                    $scope.import.items[index].name = respond.name;
                    $scope.import.items[index].size = respond.size;
                    $scope.import.items[index].brand = respond.brand;
                    $scope.import.items[index].type = "old";
                } else {
                    $scope.import.items[index].name = '';
                    $scope.import.items[index].size = '';
                    $scope.import.items[index].brand = '';
                    $scope.import.items[index].type = "new";
                }
            })
            .error(function() {
            });
    };

    /**
     * submit the form
     */
    $scope.create = function() {

        $http.post('/import', $scope.import)
            .success(function(respond) {
                if (respond.error == '') {

                    window.location = "/import/" + respond.id;

                } else {
                    alert(respond.error);
                }
            })
            .error(function() {
                alert('Internal error on creating shopping, please contact admin');
            });
    };
});

$(document).ready(function(){
    $( "#inputDate" ).datepicker({
        dateFormat: "yy-mm-dd"
    });
});