var app = angular.module('importApp', ['ui.bootstrap'], function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
});

app.controller('importCtrl', function($scope, $http, $modal) {
    $scope.data = {};

    $scope.getImport = function() {
        $http.get('/api/import/' + window.location.search)
            .success(function(respond){
                $scope.data = respond;
            })
            .error(function() {

            });
    }

    $scope.getImport();
});