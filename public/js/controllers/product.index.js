var app = angular.module('productApp', ['ui.bootstrap'], function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
});

app.controller('productCtrl', function($scope, $http, $modal) {
    $scope.data = {};

    $scope.getProduct = function() {
        $http.get('/api/product')
            .success(function(respond){
                $scope.data = respond;
            })
            .error(function() {

            });
    }

    $scope.getProduct();
});