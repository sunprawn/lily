var app = angular.module('orderApp', ['ui.bootstrap'], function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
});

app.controller('orderCtrl', function($scope, $http, $modal) {
    $scope.orderDetail = orderDetail;

    $scope.changeDev = function(order) {
        var orderId = order.id;
        var data = {};
        //data.received = $scope.data.data[$scope.data.data.indexOf(order)].received;
        data.received = order.received;

        $http.put('/order/' + orderId, data)
            .success(function(respond) {
                if (respond.error == '') {

                } else {
                    alert(respond.error);
                }
            });

    };

    $scope.enableEditor = function() {
        $scope.editorEnabled = true;
        $scope.editableMemo = $scope.orderDetail.memo;
        $("#memoTextArea").focus();
    };
    $scope.disableEditor = function() {
        $scope.orderDetail.memo = $scope.editableMemo;
        $scope.editorEnabled = false;
    };
    $scope.saveMemo = function() {
        var data = {};

        data.memo = $scope.orderDetail.memo;

        $http.put('/order/' + $scope.orderDetail.id, data)
            .success(function(respond) {
                if (respond.error == '') {

                } else {
                    alert(respond.error);
                }
                $scope.editorEnabled = false;
            })
            .error(function() {

            });


    };

});
